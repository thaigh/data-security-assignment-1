COMP3260 - Assignment 1
=======================

* Tyler Haigh - C3182929
* Simon Hartcher - C3185790

# Question 1 #

1. We start off by running Incident of Coincidence (IC) with 1 alphabet gives the result ```0.0732757```. This indicates a period of 1 or less
2. Running the graph tool with 1 alphabet generates a result that looks similar to English (comparing JKrypto result to Robert Lewand's findings in *Cryptological Mathematics* and summarised by Pavel Micka on his website [http://en.algoritmy.net/article/40379/Letter-frequency-English](http://en.algoritmy.net/article/40379/Letter-frequency-English))

![Graph of c1 Language](Images/c1-graph-1-alphabet-graphic.png)

![Graph of c1 Language](Images/english-frequency.png)

3.. Running Kasisky for tri-grams results in the following section of output

    ...
    eoe: 2| 1398
    oet: 7| 802, 54, 114, 1296, 210, 351
    eth: 13| 366, 1164, 55, 166, 13, 264, 72, 282, 78, 252, 19, 96
    the: 25| 210, 145, 11, 186, 112, 368, 30, 532, 98, 72, 204, 60, 42, 30, 212, 70, 24, 114,
             57, 82, 53, 66, 12, 307
    hen: 8| 1535, 229, 264, 212, 309, 163, 257
    era: 3| 45, 321
    ...

By examining the gcd of ```the```, we find:

    >python Gcd.py 210 145
    5
    >python Gcd.py 145 11
    1
    >python Gcd.py 11 186
    1
    >python Gcd.py 186 112
    2
    >python Gcd.py 112 3668
    28
    >python Gcd.py 112 368
    16

This doesn't give much information, so we try running Kasisky with quad-grams yielding the following section

    ...
    oecr: 2| 468
    thoe: 5| 306, 1374, 474, 606
    oeth: 2| 2827
    ethe: 7| 366, 1398, 264, 72, 282, 330
    then: 4| 1764, 264, 684
    aarc: 2| 1854
    arcn: 2| 1854
    ...

    >python Gcd.py 306 1374
    6
    >python Gcd.py 1374 474
    6
    >python Gcd.py 474 606
    6
    >python Gcd.py 366 1398
    6
    >python Gcd.py 1398 264
    6


This indicates a period of 6, The data looks like the following, formatted with a period of 6

|5|1|2|3|6|4|
|-|-|-|-|-|-|
|e|w|e|t|p|h|
|e|e|o|p|o|l|
|u|f|t|h|n|e|
|s|i|t|e|t|d|
|i|a|t|e|n|s|
|r|o|r|d|t|e|
|m|o|f|o|a|r|
|p|m|o|r|e|e|

4.. Using intuitive analysis, this is a row transposition cipher with degree 6 and column shifts ```5,1,2,3,6,4```

|1|2|3|4|5|6|
|-|-|-|-|-|-|
|w|e|t|h|e|p|
|e|o|p|l|e|o|
|f|t|h|e|u|n|
|i|t|e|d|s|t|
|a|t|e|s|i|n|

## Plain Text Message ##
    
    (Preamble)
	We the people of the United States, in order to form a more
    perfect union, establish justice, insure domestic tranquility,
    provide for the common defence (defense), promote the general welfare,
    and secure the blessings of liberty to ourselves and our
    posterity, do ordain and establish this Constitution for the
    United States of America.
    
    Article 1

	Section 1
    All legislative powers herein granted shall be vested in a Congress
	of the United States which shall consist of a Senate and House of
    Representatives.
    
    Section 2
    The House of Representatives shall be composed of members
	chosen every second year by the people of the several states,
	and the electors in each state shall have the qualifications
	requisite for electors of the most numerous branch of the
	state legislature.
	
	No person shall be a Representative who shall not have attained
	to the age of twenty five years and been seven years a citizen
	of the United States and who shall not when elected be an
	inhabitant of that state in which he shall be chosen.

	Representatives and direct taxes shall be apportioned among
	the several states which maybe included within this union
	according to their respective numbers which shall be
	determined by adding to the whole number of free persons
	including those bound to service for a term of years, and
	excluding Indians not taxed, three fifths of all
    other persons. The actual Enumeration shall be made within
    three years after the first meeting of the Congress of the
    United States and within every subsequent term of ten years, in
    such manner as they shall by law direct. The number of
    Representatives shall not exceed one for every thirty thousand,
    but each state shall have at least one Representative; and until
    such enumeration shall be made, the state of New Hampshire
    shall be entitled to chuse three, Massachusetts eight, Rhode
    Island and Providence Plantations one, Connecticut five, New
    York six, New Jersey four, Pennsylvania eight, Delaware one, Maryland
    six, Virginia ten, North Carolina five, South Carolina five, and Georgia
    three.
	
	When vacancies happen in the Representation from any
    state, the executive authority thereof shall
    issue writs of election to fill such vacancies.
	
	The House of Representatives shall chuse (choose) their speaker
	and other officers; and shall have the sole power of impeachment.
    
    Section 3
    The Senate of the United States shall be composed of two
    Senators from each state, chosen by the legislature thereof for six
    years; and each Senator shall have one vote.
	
	Immediately after they shall be assembled in consequence of the first election,
    they shall be divided as equally as may be in to three classes.
    The seats of the Senators of the first class shall be vacated
    at the expiration of the second year, of the second class at the
    expiration of the fourth year, and the third class at the expiration
    of the sixth year, so that one third may be chosen every second
    year; and if vacancies happen by resignation, or otherwise, during
    the recess of the legislature of any state, the executive thereof may
    make temporary appointments until the next meeting of the legislature,
    which shall then fill such vacancies.

	No person shall be a Senator who shall not have attained to
	the age of thirty years, and been nine years a citizen of
	the United States and who shall not, when elected, be an
	inhabitant of that state for which he shall be chosen.

	The Vice President of the United States shall
    be President of the Senate but shall have no vote, unless they be
    equally divided.

	The senate shall chuse (choose) their other officers,
    and also a President pro tempore, in the absence of the Vice
    President, or when he shall exercise the office of President of
    the United States.

	The Senate shall have the sole power to try all 
    impeachments. When sitting for that purpose they shall be on oath or
    affirmation. When the President of the United States is tried, the
    Chief Justice shall preside: And no person shall be convicted without
    the concurrence of two thirds of the members present.

	Judgment in cases of impeachment shall not extend further than to
    removal from office, and disqualification to hold and enjoy any 
    office of honor, trust or profit under the United States: but
    the party convicted shall never the less be liable and subject
    to indictment, trial, judgment and punishment, according to awl (law).

# Appendix A - Kasiski Output #

    Kasiski search for repeated strings of size 4.
    
    heeo: 2| 484
    thne: 2| 3372
    hnes: 2| 3372
    nesi: 2| 3372
    esit: 2| 3372
    site: 2| 3372
    itet: 2| 3372
    tetd: 3| 2178, 1194
    aten: 2| 486
    tens: 3| 486, 2536
    temo: 3| 1201, 1270
    emof: 2| 1201
    reet: 2| 2370
    stir: 2| 2228
    urit: 2| 2312
    oecr: 2| 468
    thoe: 5| 306, 1374, 474, 606
    oeth: 2| 2827
    ethe: 7| 366, 1398, 264, 72, 282, 330
    then: 4| 1764, 264, 684
    aarc: 2| 1854
    arcn: 2| 1854
    rcnd: 2| 1854
    suee: 2| 1074
    eret: 2| 1892
    tetr: 2| 1285
    uott: 2| 1494
    teun: 2| 2514
    eune: 2| 2514
    unei: 2| 2514
    neit: 2| 2514
    eitd: 2| 2514
    itds: 2| 2514
    tdst: 2| 2514
    dste: 2| 2514
    stea: 4| 360, 270, 1884
    eaar: 3| 442, 1296
    isec: 2| 132
    seco: 2| 132
    ecot: 2| 132
    alel: 3| 2286, 612
    lela: 2| 2286
    oive: 2| 1338
    ivew: 2| 1248
    snte: 3| 893, 1458
    tehd: 2| 1957
    eall: 6| 492, 42, 282, 282, 690
    esti: 4| 802, 282, 903
    cgoo: 2| 852
    goor: 2| 852
    oore: 3| 852, 2113
    ores: 2| 852
    resf: 2| 852
    esfs: 2| 852
    sfsn: 2| 852
    fsnt: 2| 852
    snth: 3| 571, 281
    nthe: 3| 852, 1458
    thei: 5| 454, 398, 1344, 114
    heiu: 3| 852, 1458
    eiut: 3| 852, 1458
    iutt: 3| 852, 1458
    utte: 3| 852, 1458
    tted: 3| 852, 1458
    teda: 3| 852, 1458
    edas: 3| 852, 1458
    tesi: 2| 2777
    achs: 2| 1596
    slhs: 2| 1452
    lhsl: 2| 1452
    hslc: 2| 1452
    nast: 2| 2631
    stos: 2| 212
    eena: 2| 2622
    atun: 2| 243
    prnr: 4| 252, 732, 1860
    rnre: 4| 252, 732, 1860
    nres: 4| 252, 732, 1860
    rest: 4| 252, 732, 1860
    este: 5| 215, 37, 732, 1860
    stee: 3| 252, 732
    teea: 3| 252, 732
    eeat: 3| 252, 732
    eati: 3| 252, 732
    othn: 2| 1800
    hoef: 2| 1374
    oefu: 2| 1374
    efus: 2| 1374
    fuse: 2| 1374
    user: 2| 1374
    sero: 2| 1374
    eros: 2| 1374
    rose: 2| 1374
    osep: 2| 1374
    sepr: 5| 396, 870, 108, 1128
    epre: 4| 396, 870, 108
    pree: 4| 396, 870, 108
    reei: 4| 396, 870, 108
    eein: 6| 396, 628, 242, 108, 829
    eint: 6| 396, 628, 231, 11, 108
    inta: 4| 396, 870, 108
    ntav: 3| 396, 978
    tavt: 3| 396, 978
    avta: 2| 1374
    vtae: 2| 1374
    taes: 3| 1374, 108
    aess: 3| 1374, 108
    essl: 3| 1374, 108
    sslh: 4| 1374, 108, 264
    slho: 2| 1482
    lhol: 2| 1482
    holb: 2| 1482
    olbe: 2| 1482
    lbem: 2| 1482
    bemc: 2| 1482
    emcd: 2| 1482
    mcdp: 2| 1482
    cdpo: 2| 1482
    dpos: 2| 1482
    poso: 2| 1482
    osoe: 2| 1482
    shee: 2| 2532
    heen: 3| 2475, 57
    eene: 2| 1611
    ener: 2| 538
    thee: 4| 197, 2107, 438
    eeth: 2| 1219
    aate: 2| 1686
    ense: 2| 200
    aest: 3| 593, 282
    esta: 4| 209, 685, 1284
    asth: 2| 2466
    sthh: 2| 2466
    thha: 2| 2466
    hhal: 2| 2466
    hala: 2| 2466
    alal: 2| 2466
    lale: 2| 2466
    alev: 2| 2466
    leve: 2| 2466
    evet: 2| 2466
    atni: 3| 1717, 113
    toec: 2| 1520
    rnum: 3| 528, 282
    ohft: 2| 2148
    hfte: 2| 2148
    test: 2| 815
    teas: 2| 2237
    sleg: 2| 1356
    legl: 2| 1356
    egli: 2| 1356
    glie: 2| 1356
    liea: 2| 1356
    ieat: 2| 1356
    eatu: 2| 1356
    peor: 2| 474
    shla: 8| 810, 408, 162, 36, 480, 240, 30
    hlae: 2| 2136
    laeb: 2| 2136
    aebe: 2| 2136
    atiw: 2| 2442
    heea: 2| 122
    eoft: 3| 42, 1350
    syea: 3| 540, 756
    yeaa: 2| 1296
    evye: 2| 612
    aear: 2| 1902
    seit: 2| 2026
    taeo: 2| 625
    aeon: 2| 625
    wshn: 2| 1830
    shnh: 2| 1830
    hnha: 2| 1830
    nhal: 3| 552, 1278
    halo: 3| 552, 1278
    alol: 3| 552, 1278
    whee: 2| 2177
    heee: 2| 1099
    nnth: 2| 1873
    itnt: 2| 1968
    tnto: 2| 1968
    ntoh: 2| 1968
    tohf: 2| 1968
    tthe: 2| 1913
    hein: 2| 1742
    chhh: 2| 1917
    hhhe: 2| 1918
    rese: 3| 1929, 414
    axeh: 2| 234
    ehse: 2| 1291
    irao: 2| 1347
    cswh: 2| 66
    swhh: 2| 66
    whhi: 3| 66, 189
    ncle: 2| 613
    ndhg: 2| 54
    dhgt: 2| 54
    hgto: 2| 54
    gtoe: 2| 54
    toet: 3| 54, 1620
    oets: 2| 168
    ecte: 2| 336
    enum: 2| 336
    numr: 2| 336
    umrb: 2| 336
    ilsh: 2| 2196
    lsha: 4| 1986, 210, 264
    beiy: 2| 1404
    oete: 2| 1410
    oeef: 2| 1872
    eeff: 3| 87, 1785
    ners: 3| 1554, 612
    erss: 3| 1554, 612
    rsso: 3| 1554, 612
    incd: 2| 2443
    oerr: 2| 282
    meao: 2| 1421
    yree: 2| 102
    rees: 2| 102
    eesa: 2| 102
    ffth: 3| 1081, 90
    enst: 2| 1939
    earn: 2| 1503
    numa: 2| 282
    umae: 2| 282
    maes: 2| 282
    stio: 3| 282, 1641
    tioh: 2| 282
    iohn: 2| 282
    ohne: 2| 282
    hnea: 2| 282
    neal: 2| 282
    allm: 2| 282
    llmb: 2| 282
    ithe: 2| 1758
    herf: 2| 821
    erfe: 2| 821
    dasn: 2| 1458
    asnt: 3| 1235, 223
    ntes: 2| 1458
    tesd: 2| 1458
    esda: 2| 1458
    yeve: 2| 1128
    eves: 2| 1128
    vesr: 2| 1128
    bhal: 3| 1302, 612
    lila: 2| 2169
    etes: 2| 131
    sean: 2| 2146
    nsti: 2| 1749
    lole: 2| 1278
    olet: 2| 1278
    stho: 2| 451
    thoa: 2| 985
    ndbe: 2| 1283
    haav: 2| 1230
    leta: 2| 146
    eprn: 2| 1860
    taot: 2| 270
    aoth: 2| 780
    lesh: 2| 1375
    hlat: 2| 408
    eode: 2| 1472
    ntia: 2| 1733
    tian: 2| 1733
    conc: 2| 1656
    inat: 2| 1482
    afli: 2| 1684
    aget: 2| 951
    geth: 2| 852
    ethr: 2| 1201
    npei: 2| 1800
    trhs: 2| 594
    stte: 2| 1278
    ttee: 2| 1278
    fere: 2| 720
    oect: 3| 126, 204
    ectn: 3| 126, 204
    ctni: 3| 126, 204
    chst: 2| 170
    urth: 2| 1561
    thes: 3| 885, 201
    ieth: 2| 967
    hens: 3| 264, 684
    atet: 2| 294
    toih: 3| 1074, 138
    oihe: 3| 1074, 138
    iheu: 3| 1074, 138
    heut: 3| 1074, 138
    eutn: 3| 1074, 138
    utna: 3| 1074, 138
    tnae: 3| 1074, 138
    naed: 3| 1074, 138
    aeds: 3| 1074, 138
    edst: 3| 1074, 138
    dstt: 3| 1074, 138
    ttae: 2| 681
    oenc: 2| 843
    eivi: 2| 726
    ivid: 2| 726
    vidd: 2| 726
    asea: 2| 1324
    clsa: 2| 144
    lsae: 2| 144
    eest: 2| 846
    ests: 2| 846
    stsh: 2| 846
    tsho: 2| 864
    sato: 2| 154
    thec: 3| 212, 478
    nati: 2| 840
    tnih: 2| 113
    etrh: 2| 48
    onto: 2| 1030
    hest: 2| 400
    otho: 2| 546
    inet: 2| 158
    ateu: 2| 965
    ntta: 2| 157
    fthe: 3| 253, 438
    pnne: 2| 612
    nner: 2| 612
    ssob: 2| 612
    sobh: 2| 612
    obha: 2| 612
    hale: 2| 612
    enee: 2| 53
    olhe: 2| 658
    hene: 2| 420
    oftt: 2| 510
    dres: 2| 414
    esei: 2| 414
    shie: 2| 277
    cfei: 2| 48
    nrth: 2| 357
    ipre: 2| 138
    pred: 2| 138
    reds: 2| 138
    edsf: 2| 138
    dsfe: 2| 138
    sfen: 2| 138
    fent: 2| 138
    entt: 2| 138
    ntto: 2| 138
    ttoi: 2| 138
    ioin: 2| 403
    coff: 2| 42
    offe: 2| 42
    ffei: 2| 42