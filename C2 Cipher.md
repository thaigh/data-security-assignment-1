COMP3260 - Assignment 1
=======================

* Tyler Haigh - C3182929
* Simon Hartcher - C3185790

# Question 2 #

1. Run IC with 1 alphabet - Result = 0.041553583
2. Run Graph with 1 alphabet - Result does not look like english or some substtitution of letters which correspond to English. This is in line with the IC

    ![Graph of c2 Language](Images/c2-graph-1-alphabet-graphic.png)

3. Run Kasiski with a word length of 5: 

Results:

    Kasiski search for repeated strings of size 5.

	zaugp: 2| 468
	augpd: 2| 468
	ugpdl: 2| 468
	gpdll: 2| 468
	lyltn: 2| 333
	qhkeb: 2| 1089
	bhtqa: 2| 288
	ediog: 2| 963
	diogs: 2| 963
	iogsm: 2| 963
	ahpdn: 2| 261
	djdwo: 2| 378
	jdwoi: 2| 378
	dwoig: 2| 378
	woigt: 2| 378
	oigti: 2| 378
	igtim: 2| 378
	vzuae: 2| 180
	sxfza: 2| 765
	xfzap: 2| 765
	fzapj: 2| 765
	zapji: 2| 765
	apjip: 2| 765
	pjipb: 2| 765
	ksuqo: 2| 540
	suqon: 2| 540
	uqonx: 2| 540
	qonxk: 2| 540
	onxkt: 2| 540
	nxktl: 2| 540
	vzbgg: 2| 243
	zbggs: 2| 243
	bggsx: 2| 243
	ggsxw: 2| 243
	pprvz: 2| 306
	prvzb: 2| 306
	hkebp: 2| 63
	kebpo: 2| 63

4.. Calculate `gcd(1089,765) = 9`

5.. Run IC with 9 alphabets:

	
	Computing IC values over 9 alphabets.

	IC [0] is 0.05945337
	IC [1] is 0.06558185
	IC [2] is 0.06403212
	IC [3] is 0.06706114
	IC [4] is 0.06494787
	IC [5] is 0.06896309
	IC [6] is 0.06797689
	IC [7] is 0.06936128
	IC [8] is 0.06857713


	Average is: 0.06621719
	
	
Which indicates that 9 is the correct number of alphabets.

6.. Graph with 9 alphabets
	
7.. Solving Alphabet 0

![Graph of c2 - Alphabet 0](Images/c2-graph-2-alphabet-0.png)

Looking at the frequency distribution of Alphabet 0 it is likely a Beaufort cipher. 
Reversing the graph displays values more characteristic of the English language.
After reversing the alphabet and shifting right once we getting the following:

`w -> d -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z| Plaintext
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a| 
|a|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b| Ciphertext

A0 key is `a`

8.. Solving Alphabet 1

![Graph of c2 - Alphabet 1](Images/c2-graph-2-alphabet-1.png)

Shift right 18 times

`n -> m -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z| Plaintext
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|t|s| Ciphertext

A1 key is `r`

9.. Alphabet 2

![Graph of c2 - Alphabet 2](Images/c2-graph-2-alphabet-2.png)

This one was particularly confusing as it doesn't seem to match any 
regular frequency distribution. Guess by mapping the highest to `e`

`e -> v -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z| Plaintext
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j| Ciphertext

A2 key is `i`

10.. Alphabet 3

![Graph of c2 - Alphabet 3](Images/c2-graph-2-alphabet-3.png)

`e -> o`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|t|

A3 key is `s`

11.. Alphabet 4

![Graph of c2 - Alphabet 4](Images/c2-graph-2-alphabet-4.png)

`p -> k -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|

A4 key is `t`

12.. Alphabet 5

![Graph of c2 - Alphabet 5](Images/c2-graph-2-alphabet-5.png)

`k -> p -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|t|s|r|q|p|

A5 key is `o`

13.. Alphabet 6 

![Graph of c2 - Alphabet 6](Images/c2-graph-2-alphabet-6.png)

`p -> k -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|

A6 key is `t`

14.. Alphabet 7 

![Graph of c2 - Alphabet 7](Images/c2-graph-2-alphabet-7.png)

`h -> s -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|l|k|j|i|h|g|f|e|d|c|b|a|z|y|x|w|v|u|t|s|r|q|p|o|n|m|

A7 key is `l`

15.. Alphabet 8 

![Graph of c2 - Alphabet 8](Images/c2-graph-2-alphabet-8.png)

`a -> z -> e`

|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|e|d|c|b|a|
|e|d|c|b|a|z|y|x|w|v|u|t|s|r|q|p|o|n|m|l|k|j|i|h|g|f|

A8 key is `e`

16.. Using the key `aristotle` we decrypt the cipher using Beaufort:

    bromeliad sareavari edgroupof organisms adaptedto
    anumberof climatesf oliagetak edifferen tshapesfr
    omneedlet hintobroa dandflats ymmetrica ltoirregu
    larspikya ndsoftthe foliagewh ichusuall ygrowsina
    rosetteis themostwi delypatte rnedandco loredofan
    yplantint heworldle afcolorsr angefromm aroonthro
    ughshades ofgreento goldvarie tiesmayha veleavesw
    ithredyel lowwhitea ndcreamva riegation sothersma
    ybespotte dwithpurp leredorcr eamwhileo thershave
    different colorsont hetopsand bottomsof theleaves
    theplants withinthe bromeliac eaeareabl etolivein
    avastarra yofenviro nmentalco nditionsd uetotheir
    manyadapt ationstri chomesint heformofs calesorha
    irsallowb romeliads tocapture waterincl oudforest
    sandhelpt oreflects unlightin desertenv ironments
    somebrome liadshave alsodevel opedanada ptationkn
    ownasthet ankhabitw hichinvol vesthebro meliadsfo
    rmingatig htlybound structure withtheir leavestha
    thelpstoc apturewat erandnutr ientsinth eabsenceo
    fawelldev elopedroo tsystembr omeliadsa lsousecra
    ssulacean acidmetab olismphot osynthesi stocreate
    sugarsthi sadaptati onallowsb romeliads inhotordr
    yclimates toopenthe irstomate satnightr atherthan
    duringthe daywhichp reventsth emfromlos ingwaterp
    lantsinth ebromelia ceaefamil yarewidel yrepresen
    tedinthei rnaturalc limatesac rosstheam ericasone
    speciesca nbefoundi nafricath eycanbefo undatalti
    tudesfrom sealevelt ofourthou sandtwohu ndredmete
    rsfromrai nforestst odesertso nlyonebro meliadthe
    pineapple isacommer ciallyimp ortantfoo dcropmany
    otherbrom eliadsare popularor namentalp lantsgrow
    nasbothga rdenandho useplants therearea lsoartifi
    cialbrome liadswhic hcanbeuse dasanalte rnativeto
    therealon esforpurp osesofdec orating

17.. Cleaning this up we get the following text:

> Bromeliads are a varied group of organisms adapted to a number of climates. Foliage take different shapes from needle thin to broad and flat, symmetrical to irregular, spiky and soft. The foliage which usually grows in a rosette is the most widely patterned and colored of any plant in the world. Leaf colors range from maroon, through shades of green to gold. Varieties may have leaves with red, yellow, white and cream variegations, others may be spotted with purple, red or cream. While others have different colors on the tops and bottoms of the leaves, the plants within the bromeliaceae are able to live in a vast array of environmental conditions due to their many adaptations. Trichomes, in the form of scales or hairs, allow bromeliads to capture water in cloud forests and help to reflect sunlight in desert environments. Some bromeliads have also developed an adaptation known as the tank habit, which involves the bromeliads forming a tightly bound structure with their leaves that helps to capture water and nutrients in the absence of a well-developed root system. Bromeliads also use crassulacean acid metabolism photosynthesis to create sugars. This adaptation allows bromeliads in hot or dry climates to open their stomates at night rather than during the day, which prevents them from losing water. Plants in the bromeliaceae family are widely represented in their natural climates across the Americas, one species can be found in africa, they can be found at altitudes from sea level to four-thousand-two-hundred meters, from rainforests to deserts. Only one bromeliad, the pineapple, is a commercially important food crop. Many other bromeliads are popular ornamental plants grown as both garden and houseplants. There are also artificial bromeliads which can be used as an alternative to the real ones for purposes of decorating.