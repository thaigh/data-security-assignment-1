COMP3260 - Assignment 1
=======================

* Tyler Haigh - C3182929
* Simon Hartcher - C3185790

# Question 3 #

1. Start by running Incident of Coincidence (IC) with 1 alphabet which gives the result ```0.066433676```, indicating a period of approximately 1
2. Running Graph with 1 alphabet gives a result looks like the English language with a substitution cipher applied

![Graph of c3 Language](Images/c3-graph-1-alphabet-graphic.png)

3.. Running Kasiski for trigrams gives the following section result (See Appendix A for full output)

    Kasiski search for repeated strings of size 3.
    
    wgm: 32| 93, 26, 34, 44, 71, 176, 67, 47, 19, 36, 25, 34, 12, 73, 81, 88, 8, 23, 29, 8, 24,
             22, 56, 13, 30, 12, 62, 36, 198, 99, 227
    gmq: 2| 934
    mqd: 2| 1076
    nzm: 2| 1216
    arv: 18| 381, 43, 80, 16, 73, 47, 43, 13, 66, 13, 175, 35, 26, 22, 13, 54, 206
    rvq: 18| 381, 43, 80, 16, 73, 47, 43, 13, 66, 13, 175, 35, 26, 22, 13, 54, 206
    vqw: 18| 381, 43, 80, 16, 73, 47, 43, 13, 66, 13, 175, 35, 26, 22, 13, 54, 206
    qwn: 7| 381, 139, 120, 345, 115, 206
    wno: 6| 381, 259, 266, 60, 134
    nor: 4| 381, 259, 460
    ors: 4| 381, 259, 460
    rsq: 3| 381, 935
    sqg: 2| 381
    qgv: 2| 381
    vuz: 4| 892, 303, 36
    uzw: 3| 518, 1179
    ...

4.. Running gcd for Kasiski Outputs:

    >python Gcd.py 93 26
    1
    >python Gcd.py 26 34
    2
    >python Gcd.py 34 44
    2
    >python Gcd.py 44 71
    1
    >python Gcd.py 71 176
    1
    >python Gcd.py 176 67
    1
    >python Gcd.py 67 47
    1
    >python Gcd.py 47 19
    1
    >python Gcd.py 19 36
    1

This indicates a period of 1 resulting in a mono-alphabetic substitution cipher. By examining the generated graph, shown before, we can guess that **e** is enciphered as **m**, and **t** is enciphered as **w**, as they have the highest frequency.

4.. Looking at the most common trigram ```wgm: 32| 93, 26, 34,...```, if **e** and **t** were enciphered as **m** and **w** respectively, then **h** must be enciphered as **g**. This results in ```the``` being enciphered as ```wgm```.

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext|  |  |  |  | m|  |  | g|  |  |  |  |  |  |  |  |  |  |  | w|  |  |  |  |  |  |
|Value     |  |  |  |  |12|  |  | 6|  |  |  |  |  |  |  |  |  |  |  |22|  |  |  |  |  |  |

5.. Runing Kasiski with quad-grams, now yields the result ```THsT: 4| 432, 861, 450``` which would result in **a** being enciphered as **s** to give ```THAT```

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext| s|  |  |  | m|  |  | g|  |  |  |  |  |  |  |  |  |  |  | w|  |  |  |  |  |  |
|Value     |18|  |  |  |12|  |  | 6|  |  |  |  |  |  |  |  |  |  |  |22|  |  |  |  |  |  |

6.. Running Kasiksi with digrams results in ```zz: 14| 64, 32, 164, 132, 68, 57, 41, 241, 511, 47, 110, 179, 39```.

Scott Bryce has listed the most common double letters in order of their freqency. Taken from [http://scottbryce.com/cryptograms/stats.htm](http://scottbryce.com/cryptograms/stats.htm)
> SS, EE, TT, FF, LL, MM, OO

Using this, we guess **s** is enciphered as **z**

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext| s|  |  |  | m|  |  | g|  |  |  |  |  |  |  |  |  |  | z| w|  |  |  |  |  |  |
|Value     |18|  |  |  |12|  |  | 6|  |  |  |  |  |  |  |  |  |  |25|22|  |  |  |  |  |  |

7.. Looking at the current cipher text, we can see ```HAjAvTHATAaaESSTnuT``` which could be separated out to be ```HAjAv THAT AaaESS TnuT```. One possible encipherment for **a** is **c** which would give ```HAjAv THAT ACCESS TnuT```. 

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext| s|  | a|  | m|  |  | g|  |  |  |  |  |  |  |  |  |  | z| w|  |  |  |  |  |  |
|Value     |18|  | 0|  |12|  |  | 6|  |  |  |  |  |  |  |  |  |  |25|22|  |  |  |  |  |  |

Upon replacing all **a** with **c** in the cipher text, the result looks like it works. Referring to Appedix B, a few potential words can be found

* **CrvqT**norAqHv
* TrAlSpuT
* TrAlSpuS
* El**CrvqT**ulo
* AkonruTHp
* cE**CrvqT**ulo

8.. The text ```CrvqT``` appears 18 times (via Kasiski). Considering the context of the message, ```CrvqT``` should map to ```CRYPT```. From here, more words can be identified

* ```CRYPTnoRAPHY``` --> ```CRYPTOGRAPHY```
* ```CuPHERS``` --> ```CIPHERS```
* ```ElCRYPT``` --> ```ENCRYPT```
* ```ulTERCEPT``` --> ```INTERCEPT```
* ```pESSAoE``` --> ```MESSAGE```

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext| s|  | a|  | m|  | o| g| u|  |  |  | p| l| n| q|  | r| z| w|  |  |  |  | v|  |
|Value     |18|  | 0|  |12|  |14| 6|20|  |  |  |15|11|13|16|  |17|25|22|  |  |  |  |21|  |

The resulting ciphertext (see Appendix C) enables us to identify more plaintext words:

* ```INfORMATION``` --> ```INFORMATION```
* ```SdCH``` --> ```SUCH```
* ```jITH``` --> ```WITH```
* ```RESTRICTEc``` --> ```RESTRICTED```
* ```CEkkPHONES``` --> ```CELLPHONES```
* ```EAxEScROPPING``` --> ```EAVESDROPPING```
* ```SCRAMbkING``` --> ```SCRAMBLING```
* ```TEyT``` --> ```TEXT```
* ```TAtEN``` --> ```TAKEN```
* ```SdbSEidENT``` --> ```SUBSEQUENT```
* ```REAkIeING``` --> ```REALIZING```

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext| s| b| a| c| m| f| o| g| u|  | t| k| p| l| n| q| i| r| z| w| d| x| j| y| v| e|
|Value     |18| 1| 0| 2|12| 5|14| 6|20|  |19|10|15|11|13|16| 8|17|25|22| 3|23| 9|24|21| 5|

9.. With the cipertext now (mostly) broken, we need to identify the remaining substitution. The only plaintext character that has not been mapped to its corresponding ciphertext character is **j**, which must be enciphered as **h**, as it is the only remaining unmapped ciphertext character. Hence the final substitutions are given by the following table resulting the cipher key: ```sbacmfoguhtkplnqirzwdxjyve```

|          |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Plaintext | A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value     | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Ciphertext| s| b| a| c| m| f| o| g| u| h| t| k| p| l| n| q| i| r| z| w| d| x| j| y| v| e|
|Value     |18| 1| 0| 2|12| 5|14| 6|20| 7|19|10|15|11|13|16| 8|17|25|22| 3|23| 9|24|21| 5|

# The Plain Text #

    The purpose of cryptography is to transmit information in
    such a way that access to it is restricted entirely to the
    intended recipient, even if the transmission itself is received
    by others.  This science is of increasing importance with the
    advent of broadcast and network communication, such as 
    electronic transactions, the Internet, e-mail, and cell phones,
    where sensitive monetary, business, political, and personal
    communications are transmitted over public channels.

    Cryptography operates by a sender scrambling or encrypting the original
    message or plaintext in a systematic way that obscures
    its meaning. The encrypted message or crypto text is transmitted,
    and the receiver recovers the message by unscrambling or
    decrypting the transmission.

    Originally the security of a cryptogram depended on the
    secrecy of the entire encrypting and decrypting procedures.
    Today however we use ciphers in which the algorithm for encrypting
    and decrypting could be revealed to any body without compromising
    the security of a particular message. In such ciphers a set of specific
    parameters, called a key, is used together with the plaintext as an
    input to the encrypting algorithm, and together with the 
    crypto text as an input to the decrypting algorithm. The encrypting
    and decrypting algorithms are publicly announced; the security
    of the cryptogram depends entirely on the secrecy of the key.
    To prevent this being discovered by accident or systematic
    search the key is chosen as a very large number.

    Once the key is established, subsequent secure communication can
    take place by sending crypto text, even over a public channel
    that is vulnerable to total passive eavesdropping, such as
    public announcements in mass media. However to establish the
    key, two users, who may not be in contact or share any secret
    information initially, will have to discuss it, using some other
    reliable and secure channel. But since interception is a set
    of measurements performed by an eavesdropper on a channel,
    however difficult this might be from a technological point 
    of view, any classical key distribution can in principle be
    passively monitored, without the legitimate users realizing
    that any eavesdropping has taken place.

# Appendix A - Kasiski Output #

    Kasiski search for repeated strings of size 3.
    
    wgm: 32| 93, 26, 34, 44, 71, 176, 67, 47, 19, 36, 25, 34, 12, 73, 81, 88, 8, 23, 29, 8, 24, 
             22, 56, 13, 30, 12, 62, 36, 198, 99, 227
    gmq: 2| 934
    mqd: 2| 1076
    nzm: 2| 1216
    arv: 18| 381, 43, 80, 16, 73, 47, 43, 13, 66, 13, 175, 35, 26, 22, 13, 54, 206
    rvq: 18| 381, 43, 80, 16, 73, 47, 43, 13, 66, 13, 175, 35, 26, 22, 13, 54, 206
    vqw: 18| 381, 43, 80, 16, 73, 47, 43, 13, 66, 13, 175, 35, 26, 22, 13, 54, 206
    qwn: 7| 381, 139, 120, 345, 115, 206
    wno: 6| 381, 259, 266, 60, 134
    nor: 4| 381, 259, 460
    ors: 4| 381, 259, 460
    rsq: 3| 381, 935
    sqg: 2| 381
    qgv: 2| 381
    vuz: 4| 892, 303, 36
    uzw: 3| 518, 1179
    zwn: 3| 41, 659
    wnw: 9| 65, 445, 419, 46, 15, 306, 44, 2
    wrs: 6| 94, 134, 108, 180, 72
    rsl: 6| 94, 134, 108, 180, 72
    slz: 6| 94, 134, 108, 180, 72
    lzp: 5| 94, 242, 180, 72
    zpu: 6| 94, 242, 180, 72, 1048
    puw: 3| 336, 180
    uwu: 6| 35, 237, 24, 1180, 269
    wul: 13| 405, 32, 137, 90, 13, 66, 13, 175, 61, 22, 13, 434
    ulf: 2| 1461
    lfn: 2| 1461
    fnr: 4| 731, 730, 118
    nrp: 3| 1461, 118
    rps: 2| 1461
    psw: 6| 440, 721, 300, 177, 103
    swu: 8| 193, 120, 127, 721, 89, 60, 151
    wun: 8| 193, 27, 93, 937, 211, 86, 138
    unl: 10| 87, 106, 27, 93, 268, 669, 211, 86, 138
    nlu: 6| 87, 120, 1254, 86, 166
    lul: 6| 461, 442, 61, 497, 227
    ulz: 2| 822
    lzd: 3| 191, 631
    zda: 4| 191, 631, 522
    dag: 4| 191, 631, 522
    ags: 7| 191, 143, 958, 52, 172, 75
    jsv: 2| 432
    svw: 2| 432
    vwg: 3| 432, 149
    wgs: 4| 432, 861, 450
    gsw: 4| 432, 861, 450
    sws: 2| 1743
    wsa: 2| 1417
    saa: 2| 1126
    mzz: 6| 260, 132, 68, 57, 282
    wuz: 3| 471, 812
    uzr: 2| 69
    zrm: 3| 69, 1649
    rmz: 4| 227, 198, 225
    mzw: 4| 650, 533, 181
    zwr: 3| 468, 1179
    wru: 2| 1647
    uaw: 2| 176
    wmc: 4| 292, 148, 32
    mcm: 2| 939
    cml: 2| 1109
    mlw: 10| 27, 93, 484, 442, 34, 29, 83, 139, 196
    lwu: 3| 604, 442
    wur: 3| 604, 442
    urm: 3| 604, 442
    rmk: 3| 1046, 417
    mkv: 3| 1046, 620
    kvw: 2| 547
    vwn: 2| 1066
    nwg: 5| 60, 804, 61, 528
    gmu: 2| 175
    mul: 6| 175, 598, 303, 300, 110
    ulw: 6| 175, 196, 473, 642, 114
    lwm: 6| 14, 161, 196, 473, 642
    mlc: 5| 318, 248, 460, 187
    lcm: 3| 318, 248
    cmc: 2| 566
    rma: 8| 38, 419, 8, 109, 467, 137, 283
    mau: 3| 637, 148
    auq: 4| 637, 132, 866
    uml: 2| 55
    wmx: 2| 1215
    mxm: 7| 621, 69, 358, 167, 104, 220
    xml: 4| 88, 960, 167
    fwg: 4| 565, 425, 42
    gmw: 2| 494
    mwr: 3| 242, 252
    puz: 3| 494, 211
    uzz: 3| 32, 462
    zzu: 6| 494, 752, 157, 179, 39
    zun: 2| 494
    luw: 3| 1376, 250
    uwz: 2| 367
    wzm: 2| 1142
    zmk: 2| 109
    mam: 2| 419
    amu: 4| 23, 396, 1017
    mux: 2| 419
    uxm: 5| 162, 257, 812, 375
    mcb: 3| 1035, 438
    cbv: 3| 1035, 438
    gmr: 8| 144, 261, 187, 132, 49, 60, 560
    mrz: 9| 185, 80, 154, 173, 132, 24, 555, 330
    rzw: 2| 419
    zwg: 3| 110, 309
    wgu: 3| 1008, 499
    guz: 3| 1008, 499
    mla: 7| 269, 80, 179, 79, 188, 83
    lam: 6| 24, 902, 154, 164, 169
    ula: 4| 1299, 106, 161
    lar: 7| 261, 80, 179, 79, 188, 83
    arm: 4| 502, 467, 349
    rms: 3| 1310, 305
    msz: 2| 1426
    zul: 7| 142, 428, 85, 583, 120, 40
    ulo: 21| 249, 12, 67, 90, 12, 90, 13, 66, 13, 43, 132, 61, 22, 13, 109, 143, 75, 148, 259, 
             20
    wsl: 3| 28, 1587
    juw: 5| 626, 111, 60, 776
    uwg: 9| 571, 55, 111, 44, 16, 45, 35, 696
    wgw: 3| 737, 60
    gwg: 6| 560, 177, 60, 219, 234
    gms: 2| 560
    lwn: 3| 989, 504
    wnf: 4| 680, 714, 99
    szw: 2| 1607
    zws: 4| 1043, 181, 383
    slc: 9| 67, 52, 219, 148, 79, 197, 74, 505
    lmw: 3| 56, 37
    wjn: 2| 1231
    anp: 4| 120, 479, 458
    npp: 3| 120, 937
    ppd: 3| 120, 937
    pdl: 3| 120, 937
    dlu: 3| 120, 937
    lua: 4| 21, 99, 937
    uas: 7| 99, 21, 937, 113, 287, 23
    asw: 3| 120, 937
    nlz: 3| 27, 93
    gsz: 3| 1153, 424
    szm: 4| 171, 467, 714
    mkm: 2| 1529
    awr: 2| 6
    rnl: 3| 993, 394
    lzs: 2| 100
    saw: 2| 1217
    wmr: 3| 629, 682
    mwm: 2| 624
    wmp: 3| 201, 721
    mps: 3| 201, 721
    ksl: 2| 52
    nlm: 2| 19
    lmz: 2| 29
    zjg: 2| 1164
    mrm: 4| 261, 241, 380
    mzm: 6| 339, 34, 166, 258, 43
    zml: 5| 114, 712, 96, 87
    zuw: 3| 195, 1033
    pnl: 2| 1446
    dzu: 2| 1216
    wua: 4| 152, 372, 349
    ask: 4| 573, 785, 23
    qmr: 4| 67, 1208, 22
    nls: 2| 1296
    lsk: 3| 108, 181
    zsr: 2| 715
    srm: 3| 715, 409
    rmw: 2| 1132
    uww: 2| 180
    wwm: 2| 180
    mcn: 2| 295
    nxm: 4| 197, 607, 153
    xmr: 9| 190, 7, 162, 445, 50, 103, 100, 220
    qdb: 4| 700, 258, 62
    dbk: 4| 700, 258, 62
    bku: 8| 46, 169, 485, 180, 78, 62, 41
    kua: 4| 700, 258, 62
    uaa: 2| 958
    aag: 2| 958
    gsl: 4| 958, 224, 75
    sll: 6| 700, 258, 60, 164, 75
    llm: 4| 958, 224, 75
    lmk: 4| 958, 224, 75
    zar: 3| 30, 169
    mrs: 3| 927, 26
    swm: 3| 1271, 103
    bvs: 3| 772, 438
    rza: 2| 483
    ars: 2| 169
    rsp: 5| 169, 66, 239, 221
    spb: 2| 169
    pbk: 2| 169
    kul: 2| 169
    lon: 2| 169
    onr: 6| 169, 162, 210, 61, 35
    nrm: 3| 338, 992
    rml: 2| 338
    qwu: 11| 169, 90, 13, 66, 13, 175, 61, 22, 13, 528
    low: 5| 67, 102, 225, 962
    owg: 5| 67, 102, 225, 962
    mnr: 3| 15, 68
    nru: 6| 181, 135, 210, 61, 35
    ruo: 2| 181
    uou: 2| 181
    oul: 2| 181
    uls: 3| 22, 159
    pmz: 4| 68, 57, 282
    zzs: 4| 68, 57, 282
    zso: 4| 68, 57, 282
    som: 4| 68, 57, 282
    omn: 2| 68
    qks: 4| 473, 367, 524
    ksu: 2| 473
    sul: 2| 473
    wmy: 5| 69, 404, 61, 321
    myw: 5| 69, 404, 61, 321
    ywu: 2| 69
    lsz: 2| 752
    zvz: 2| 721
    vzw: 2| 721
    zwm: 2| 721
    zad: 2| 1034
    adr: 6| 147, 200, 258, 179, 283
    drm: 5| 225, 559, 283, 42
    zpm: 2| 920
    pms: 2| 1098
    msl: 3| 982, 70
    slu: 4| 442, 61, 724
    gmm: 4| 173, 273, 83
    mml: 5| 173, 6, 267, 83
    nwm: 3| 465, 321
    mcs: 2| 357
    lcw: 2| 424
    cwg: 2| 538
    mrr: 2| 981
    rrm: 2| 981
    man: 2| 713
    anx: 2| 607
    mpm: 3| 833, 196
    mbv: 2| 722
    cma: 5| 103, 79, 236, 35
    mar: 10| 72, 31, 79, 210, 26, 35, 54, 32, 349
    skk: 3| 273, 606
    kkv: 2| 879
    gmz: 5| 34, 166, 258, 43
    zma: 9| 34, 66, 100, 258, 43, 136, 213, 70
    mad: 5| 200, 258, 179, 283
    dru: 3| 200, 258
    ruw: 7| 119, 81, 129, 61, 35, 33
    uwv: 3| 200, 258
    wvn: 3| 200, 258
    vnf: 5| 33, 167, 258, 42
    nfs: 2| 200
    spc: 2| 460
    pcm: 2| 460
    cmq: 2| 460
    mqm: 2| 460
    qml: 2| 460
    nlw: 3| 467, 338
    lwg: 2| 467
    mav: 2| 467
    avn: 2| 467
    nfw: 3| 425, 42
    los: 6| 79, 188, 61, 22, 13
    osl: 3| 79, 271
    lcc: 3| 79, 271
    ccm: 3| 79, 271
    qrn: 2| 113
    amc: 2| 374
    wnc: 2| 799
    gnj: 3| 698, 220
    njm: 3| 698, 220
    jmx: 3| 698, 220
    mrj: 3| 192, 60
    mdz: 2| 1046
    dzm: 4| 178, 538, 330
    uqg: 2| 132
    qgm: 2| 132
    agw: 2| 456
    msk: 3| 46, 987
    sko: 4| 210, 61, 35
    kon: 4| 210, 61, 35
    wgp: 4| 210, 61, 35
    loa: 2| 523
    bmr: 2| 442
    rmx: 2| 358
    kmc: 2| 102
    mcw: 3| 112, 174
    cwn: 3| 112, 60
    slv: 4| 676, 217, 99
    vju: 2| 698
    wgn: 2| 947
    gnd: 2| 947
    ndw: 2| 947
    rnp: 2| 847
    qsr: 2| 44
    uad: 2| 804
    adk: 2| 804
    ksr: 2| 375
    rpm: 2| 758
    zsz: 2| 714
    zmw: 2| 714
    mwn: 4| 481, 159, 74
    zqm: 2| 725
    fua: 2| 766
    stm: 3| 389, 523
    tmv: 6| 241, 62, 36, 198, 267
    mvu: 3| 303, 36
    nom: 2| 60
    omw: 2| 60
    mwg: 3| 60, 263
    rju: 2| 60
    mqk: 2| 367
    yws: 2| 61
    wsz: 2| 61
    szs: 3| 61, 220
    zsl: 2| 61
    ulq: 3| 61, 724
    lqd: 2| 61
    qdw: 2| 61
    dww: 3| 61, 757
    wwn: 2| 61
    osk: 3| 61, 35
    gma: 2| 115
    gmc: 2| 246
    vsl: 2| 538
    lln: 2| 318
    lnd: 2| 318
    ndl: 2| 318
    dla: 2| 318
    lcz: 2| 433
    czm: 2| 433
    gmt: 4| 62, 36, 198
    mtm: 4| 62, 36, 198
    mvw: 2| 296
    wwg: 3| 499, 108
    bmu: 2| 300
    cuz: 3| 352, 193
    uza: 3| 43, 309
    rmc: 2| 581
    wnr: 3| 286, 281
    nrz: 2| 286
    sxm: 5| 152, 141, 106, 180
    mrn: 2| 394
    nla: 3| 50, 435
    wsb: 2| 181
    sbk: 4| 102, 79, 113
    kuz: 2| 181
    uzg: 2| 181
    lwz: 3| 139, 196
    las: 2| 435
    asl: 3| 107, 328
    lws: 2| 177
    wst: 2| 523
    ksa: 2| 524
    vzm: 2| 180
    bkm: 2| 192
    skq: 2| 321
    qsz: 2| 375
    szz: 4| 47, 289, 39
    zux: 2| 375
    msx: 3| 247, 180
    xmz: 3| 247, 180
    mzc: 3| 247, 180
    zcr: 3| 247, 180
    crn: 3| 247, 180
    rnq: 3| 247, 180
    nqq: 3| 247, 180
    qqu: 2| 427
    qul: 2| 427
    loz: 2| 148
    pml: 2| 196
    wzu: 2| 160
    pmc: 2| 196
    zmr: 2| 330
    nps: 2| 215
    wbm: 2| 204
    mag: 2| 116
    bdw: 2| 152


# Appendix B - Substitution Phase 1 #

    THE qdrqnSEnf CrvqTnorAqHv uSTn TrAlSpuT ulfnrpATunlulS
    dCHAjAv THAT ACCESS TnuTuS rEST ruCTEcElTurEkvTn THEulTE
    lcEcrECuquElTExEluf THE  TrAlSpuS SunluTSEkfuSrECEuxEc
    bvn THErSTH uSSCuElCEuSnfulCrEASuloupqnrTAlCEjuTH THE
    AcxEl TnfbrnAcC AS TAlclETjnrtCnppdluCATunlSdC HAS EkEC
    TrnluC  TrAlSACT unl S THE ulTErlETE pAukAlcCEkkqHnlESjHE
    rESElSuT uxEpnlETArvbdSulESSqnkuTuCAkAlcqErSnlAkCnp
    pdluCATunlSArE TrAlSpuT TEcnxErqdbkuCCHAllEkS CrvqTnorAqHv 
    nqErATESbvASElcErSCrApbkulonr ElCrvqTulo THE nru
    oulAkpESSAoEnrqkAulTEyTulASvSTEpATuCjAv THAT nbSCdrE
    SuTSpEAluloTHEElCrvq TEcpESS AoEnr CrvqT nTEyTuS TrAlSpuT 
    TEcAlcTHErECEuxErrECnxErSTHEpESSAoEbvdlSCrApbkul
    onr cECrvqTulo THE  TrAlSpuS SunlnruoulAkkv THESE CdruTvn
    fA CrvqTnorA pcEqElcEcnl THESE CrE CvnfTHEElTurE ElCrvqTulo 
    Alc cECrvqTulo qrnCEcdrESTncAvHnjExErjEdSECuqHErS
    uljHuCH THE AkonruTHp fnr ElCrvqTulo Alc cECrvqTulo Cndkc
    bErExEAkEcTnAlvbncvjuTHndTCnpqrnpuSulo THESE CdruTvn
    fAqArTuCdkArpESSAoEulSdCHCuqHErSASETnfSqECufuCqArA
    pETErSCAkkEcAtEvuSdSEcTnoETHErjuTHTHEqkAulTEyTASAl
    ulqdTTn THE El CrvqT ulo AkonruTHp AlcTnoE THE rjuTH THE Crv
    qTnTEyTASAlulqdTTnTHE cECrvqTulo AkonruTHp THE ElCrvqTulo 
    Alc cECrvqTulo AkonruTHp SArEqdbkuCkvAllndlCEcTHES
    ECdruTvnf THE CrvqTnorA pcEqElcSElTurEkvnl THESE CrE Cvn
    fTHEtEvTnqr ExElTTH uSbEulocuSCnxErEcbvACCucElTnrSvS
    TEpATuCSEArCHTHEtEvuSCHnSElASAxErvkAroEldpbErnlCET
    HEtEvuSESTAbkuSHEcSdbSEidElTSECdrECnppdluCATunlCAl
    TAtEqkACEbvSElc ulo CrvqT nTEyTExElnxErAqdbkuCCHAllEk
    THATuSxdklErAbkETnTnTAkqASSuxEEAxEScrnqquloSdCHASq
    dbkuCAllndlCEpElTSulpASSpEcuAHnjExErTnESTAbkuSH THE
    tEvTjndSErSjHnpAvlnTbEulCnlTACTnrSHArEAlvSECrETulf
    nrpATunluluTuAkkvjukkHAxETncuSCdSSuTdSuloSnpEnTHEr
    rEkuAbkEAlcSECdrECHAllEkbdTSulCEulTErCEqTunluSA SET
    nfpEASdrEpElTSqErfnrpEcbvAlEAxEScrnqqErnlACHAllEkH
    njExErcuffuCdkTTH uSpuoHTbEfrnpATE CHlnknouCAkqnulTn
    fxuEjAlvCkASSuCAktEvcuSTrubdTunlCAlulqrulCuqkEbEqA
    SSuxEkvpnluTnrEcjuTHndT THE kEouTupATE dSErSrEAkueulo
    THAT AlvEAxEScrnqquloHAS TAtElqkACE

# Appendix C - Substitution Phase 2 #

    THE PdRPOSE Of CRYPTOGRAPHY IS TO TRANSMIT INfORMATION IN 
    SdCH AjAY THAT ACCESS TO IT IS RESTRICTEc ENTIREkY TO THE 
    INTENcEc RECIPIENT ExEN If THE TRANSMISSION IT SEkfISRECEIxEc
    bY OTHERS THIS SCIENCE IS Of INCREASING IMPORTANCE jITH THE
    AcxENTOfbROAc CAST ANcNETjORt COMMdNICATIONS dCHAS
    EkECTRONIC TRANSACTIONS THE INTERNET EMAIk ANc CEkkPHONES jHE
    RESENSITIxE MONETARY bdSINESSPOkITICAkANc PERSON Ak
    COMMdNICATIONS ARE TRANSMITTEc OxERPdbkIC CHANNEkS
    CRYPTOGRAPHY OPERATES bYASENcERSCRAMbkING OR ENCRYPTING THE
    ORIGINAk MESSAGE OR PkAIN TEyT IN A SYSTEMATIC jAY THAT ObSCdRES
    ITS MEANING THE ENCRYPTEc MESSAGE OR CRYPTOTEyT IS TRANSMITTEc
    ANc THERE CEIxERRECOxERS THE MESSAGE bY dN SCRAMbkING
    OR cECRYPTING THE TRANSMISSION ORIGINAkkY THESE CdRITYOfACRY
    PTOGRAM cEPENcEc ON THE SECRECY Of THE ENTIRE ENCRYPTING
    ANc cECRYPTING PROCEcdRESTOcAYHOjExERjEdSE CIPHERS
    IN jHICH THE AkGORITHM fOR ENCRYPTING ANc cECRYPTING
    COdkcbERExEAkEc TO ANY bOcYjITHOdT COMPROMISING THESE CdRITYO
    fAPARTICdkAR MESSAGE INSdCH CIPHERS A SET Of SPECIfIC PARAMETERS
    CAkkEcAtEYISdSEc TOGETHER jITHTHE PkAIN TEyT AS ANINPdTTOTHE
    ENCRYPTING AkGORITHM ANc TOGETHER jITH THE CRYPTO TEyT AS 
    AN INPdT TO THE cECRYPTING AkGORITHM THE ENCRYPTING
    ANc cECRYPTING AkGORITHMS AREPdbkICkYANNOdNCEc
    THESE CdRITYOf THE CRYPTOGRAM cEPENcSENTIREkYONTHE
    SECRECY Of THE tEYTOPRExENTTHIS bEING cISCOxEREcbYACCIcENTOR
    SYSTEM ATIC SEARCH THE tEYISCHOSENASAxERYkARGENdMbERONCET
    HEtEYISESTAbkISHEcSdbSEidENTSECdRE COMMdNICATION CAN
    TAtEPkACEbYSENcING CRYPTO TEyT ExENOxERAPdbkIC CHANNEk
    THAT ISxdkNERAbkETOTOTAkPASSIxEEAxES cROPPING SdCHASP
    dbkICANNOdNCEMENTSINMASSMEcIAHOjExERTOESTAbk ISH THE
    tEYTjOdSERSjHOMAYNOTbE IN CONTACT OR SHARE ANY SECRET INfORMATION
    IN ITIAkkYjIkkHAxETOcISCdSSIT dSING SOMEOTHER
    REkIAbkEANcSECdRE CHANNEk bdT SINCE INTERCEPTION IS A SET
    Of MEASdREMENTS PERfORMEc bY AN EAxEScROPPERONA CHANNEk
    HOjExERcIffICdkT THIS MIGHT bE fROMATECHNOkOGICAkPOINTO
    fxIEjANYCkASSICAktEYcISTRIbdTION CAN IN PRINCIPkEbEPA
    SSIxEkY MONITO REcjITHOdT THE kEGITIMATE d SERSREAkIeING
    THAT ANY EAxEScROPPING HAS TAtEN PkACE