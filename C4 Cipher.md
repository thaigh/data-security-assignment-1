COMP3260 - Assignment 1
=======================

* Tyler Haigh - C3182929
* Simon Hartcher - C3185790

# Question 4 #

1. Running Incident of coincidence (IC) with 1 alphabet gives the result ```0.04085928``` meaning the text is not English based, but could be a substitution cipher.
2. Running Kasiski for trigrams gives the following. Refer to Appendix A for the full display:
   
Result:

    Kasiski search for repeated strings of size 3.
    
    iym: 3| 399, 378
    znk: 3| 357, 528
    nke: 3| 357, 528
    keb: 2| 357
    ebr: 2| 357
    evs: 2| 861
    roy: 2| 329
    oyr: 2| 329
    yry: 2| 329
    agp: 2| 399
    gpx: 2| 399
    pxo: 2| 399
    svk: 3| 476, 385
    vkm: 4| 7, 469, 385
    ...
    
3.. Running GCD for these outputs generates the following result:

    >python Gcd.py 399 378
    21
    >python Gcd.py 357 528
    3
    >python Gcd.py 357 861
    21
    >python Gcd.py 357 329
    7
    >python Gcd.py 329 861
    7
    >python Gcd.py 7 469
    7
    >python Gcd.py 7 358
    1

4.. Using this, we guess the period for the language is around 7. We try this using IC to verify:

For 6 alphabets:

    Computing IC values over 6 alphabets.
    
    IC [0] is 0.041935485
    IC [1] is 0.038047973
    IC [2] is 0.037717123
    IC [3] is 0.041025642
    IC [4] is 0.04127378
    IC [5] is 0.040217847
    
    Average is: 0.040036306

For 7 alphabets:

    Computing IC values over 7 alphabets.
    
    IC [0] is 0.06823028
    IC [1] is 0.05981371
    IC [2] is 0.057681516
    IC [3] is 0.06329256
    IC [4] is 0.07006152
    IC [5] is 0.058441557
    IC [6] is 0.06630212
    
    Average is: 0.06340332

For 8 alphabets:

    Computing IC values over 8 alphabets.
    
    IC [0] is 0.04199823
    IC [1] is 0.04067197
    IC [2] is 0.03521957
    IC [3] is 0.04008252
    IC [4] is 0.04140878
    IC [5] is 0.036545828
    IC [6] is 0.04597701
    IC [7] is 0.04227886

    Average is: 0.040522844

Now that we have verified that the period is 7. We can guess that this is a poly-alphabetic cipher with 7 alphabets as shown below:

![Graph of c4 Language Alphabet 0](Images/c4-graph-7-alphabet-0.png)

![Graph of c4 Language Alphabet 1](Images/c4-graph-7-alphabet-1.png)

![Graph of c4 Language Alphabet 2](Images/c4-graph-7-alphabet-2.png)

![Graph of c4 Language Alphabet 3](Images/c4-graph-7-alphabet-3.png)

![Graph of c4 Language Alphabet 4](Images/c4-graph-7-alphabet-4.png)

![Graph of c4 Language Alphabet 5](Images/c4-graph-7-alphabet-5.png)

![Graph of c4 Language Alphabet 6](Images/c4-graph-7-alphabet-6.png)


The general distribution of letter frequencies seems to indicate the use of a Vigenere Cipher. This will now be tested.

## Alphabet 0 ##

By examining the most frequent letters of the alphabet, g, p, and t are all candiates for being the enciphered letter for E

|           |  |  |  |  |      |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|-----------|--|--|--|--|------|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Ciphertext | a| b| c| d|   e  | f| g| h| **i**| j| k| l| m| n| o| p| q| r| s| t| u| v| w| x| y| z|
|Value      | 0| 1| 2| 3|   4  | 5| 6| 7| **8**| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Shift G > e| C| D| E| F| **G**| H| I| J|   K  | L| M| N| O| P| Q| R| S| T| U| V| W| X| Y| Z| A| B|
|Value      | 2| 3| 4| 5| **6**| 7| 8| 9|  10  |11|12|13|14|15|16|17|18|19|20|21|22|23|24|25| 0| 1|
|Shift P > e| L| M| N| O| **P**| Q| R| S|   T  | U| V| W| X| Y| Z| A| B| C| D| E| F| G| H| I| J| K|
|Value      |11|12|13|14|**15**|16|17|18|  19  |20|21|22|23|24|25| 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|
|Shift T > e| P| Q| R| S| **T**| U| V| W|   X  | Y| Z| A| B| C| D| E| F| G| H| I| J| K| L| M| N| O|
|Value      |15|16|17|18|**19**|20|21|22|  23  |24|25| 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|

For Shift G -> e

    Plaintext = (Ciphertext - Key Shift)
      = (i - C) mod 26
      = (8 - 2) mod 26
      = (6) mod 26
      = G

For Shift P -> e

    Plaintext = (Ciphertext - Key Shift)
      = (i - L) mod 26
      = (8 - 11) mod 26
      = (-3) mod 26
      = 23
      = X

For Shift T -> e

    Plaintext = (Ciphertext - Key Shift)
      = (i - P) mod 26
      = (8 - 15) mod 26
      = (-7) mod 26
      = 19
      = T

## Alphabet 1 ##

By examining the most frequent letters of the alphabet, v is the most likely candidate for being enciphered from E

|           |  |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |      |  |
|-----------|--|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|------|--|
|Ciphertext | a| b| c| d|   e  | f| g| h| i| j| k| l| m| n| o| p| q| r| s| t| u| v| w| x| **y**| z|
|Value      | 0| 1| 2| 3|   4  | 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|**24**|25|
|Shift V > e| R| S| T| U| **V**| W| X| Y| Z| A| B| C| D| E| F| G| H| I| J| K| L| M| N| O|   P  | Q|
|Value      |17|18|19|20|**21**|22|23|24|25| 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|  15  |16|

For Shift V -> e

    Plaintext = (Ciphertext - Key Shift)
      = (y - R) mod 26
      = (24 - 17) mod 26
      = (7) mod 26
      = H

## Alphabet 2 ##

By examining the most frequent letters of the alphabet, m is the most likely candidate for being enciphered from E

|           |  |  |  |  |      |  |  |  |  |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |  |
|-----------|--|--|--|--|------|--|--|--|--|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Ciphertext | a| b| c| d|   e  | f| g| h| i| j| k| l| **m**| n| o| p| q| r| s| t| u| v| w| x| y| z|
|Value      | 0| 1| 2| 3|   4  | 5| 6| 7| 8| 9|10|11|**12**|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Shift M > e| I| J| K| L| **M**| N| O| P| Q| R| S| T|   U  | V| W| X| Y| Z| A| B| C| D| E| F| G| H|
|Value      | 8| 9|10|11|**12**|13|14|15|16|17|18|19|  20  |21|22|23|24|25| 0| 1| 2| 3| 4| 5| 6| 7|

For Shift M -> e

    Plaintext = (Ciphertext - Key Shift)
      = (m - I) mod 26
      = (12 - 8) mod 26
      = (4) mod 26
      = E

## Alphabet 3 ##

By examining the most frequent letters of the alphabet, z is the most likely candidate for being enciphered from E

|           |  |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |      |
|-----------|--|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|------|
|Ciphertext | a| b| c| d|   e  | f| g| h| i| j| k| l| m| n| o| p| q| r| s| t| u| v| w| x| y| **z**|
|Value      | 0| 1| 2| 3|   4  | 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|**25**|
|Shift M > e| V| W| X| Y| **Z**| A| B| C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T|   U  |
|Value      |21|22|23|24|**25**| 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|  20  |

For Shift Z -> e

    Plaintext = (Ciphertext - Key Shift)
      = (z - V) mod 26
      = (25 - 21) mod 26
      = (4) mod 26
      = E

## Alphabet 4 ##

By examining the most frequent letters of the alphabet, e is the most likely candidate for being enciphered from E

|           |  |  |  |  |      |  |  |  |  |  |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |
|-----------|--|--|--|--|------|--|--|--|--|--|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|
|Ciphertext | a| b| c| d|   e  | f| g| h| i| j| k| l| m| **n**| o| p| q| r| s| t| u| v| w| x| y| z|
|Value      | 0| 1| 2| 3|   4  | 5| 6| 7| 8| 9|10|11|12|**13**|14|15|16|17|18|19|20|21|22|23|24|25|
|Shift E > e| A| B| C| D| **E**| F| G| H| I| J| K| L| M|   N  | O| P| Q| R| S| T| U| V| W| X| Y| Z|
|Value      | 0| 1| 2| 3| **4**| 5| 6| 7| 8| 9|10|11|12|  13  |14|15|16|17|18|19|20|21|22|23|24|25|

For Shift E -> e

    Plaintext = (Ciphertext - Key Shift)
      = (n - A) mod 26
      = (13 - 0) mod 26
      = (13) mod 26
      = N

## Alphabet 5 ##

By examining the most frequent letters of the alphabet, g, and v are the most likely candidates for being enciphered from E

|           |  |  |  |  |      |  |  |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|-----------|--|--|--|--|------|--|--|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Ciphertext | a| b| c| d|   e  | f| g| h| i| j| **k**| l| m| n| o| p| q| r| s| t| u| v| w| x| y| z|
|Value      | 0| 1| 2| 3|   4  | 5| 6| 7| 8| 9|**10**|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Shift G > e| C| D| E| F| **G**| H| I| J| K| L|   M  | N| O| P| Q| R| S| T| U| V| W| X| Y| Z| A| B|
|Value      | 2| 3| 4| 5| **6**| 7| 8| 9|10|11|  12  |13|14|15|16|17|18|19|20|21|22|23|24|25| 0| 1|
|Shift V > e| R| S| T| U| **V**| W| X| Y| Z| A|   B  | C| D| E| F| G| H| I| J| K| L| M| N| O| P| Q|
|Value      |17|18|19|20|**21**|22|23|24|25| 0|   1  | 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|

For Shift G -> e

    Plaintext = (Ciphertext - Key Shift)
      = (k - C) mod 26
      = (10 - 2) mod 26
      = (8) mod 26
      = I

For Shift V -> e

    Plaintext = (Ciphertext - Key Shift)
      = (k - R) mod 26
      = (10 - 17) mod 26
      = (-7) mod 26
      = 19
      = T

## Alphabet 6 ##

By examining the most frequent letters of the alphabet, c is the most likely candidate for being enciphered from E

|           |  |  |  |  |      |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|-----------|--|--|--|--|------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|Ciphertext | a| b| c| d| **e**| f| g| h| i| j| k| l| m| n| o| p| q| r| s| t| u| v| w| x| y| z|
|Value      | 0| 1| 2| 3| **4**| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|Shift C > e| Y| Z| A| B| **C**| D| E| F| G| H| I| J| K| L| M| N| O| P| Q| R| S| T| U| V| W| X|
|Value      |24|25| 0| 1| **2**| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|

For Shift C -> e

    Plaintext = (Ciphertext - Key Shift)
      = (e - Y) mod 26
      = (4 - 24) mod 26
      = (-20) mod 26
      = 6
      = G

# Plain Text #

From analysing each alphabet in the language, we can see the potential keys and their corresponding plain text for the first seven letters is:

|Alphabet     | 0| 1| 2| 3| 4| 5| 6|
|-------------|--|--|--|--|--|--|--|
|Key: crivacy | G| H| E| E| N| I| G|
|Key: crivary | G| H| E| E| N| T| G|
|Key: lrivacy | X| H| E| E| N| I| G|
|Key: lrivary | X| H| E| E| N| T| G|
|**Key: privacy** | **T**| **H**| **E**| **E**| **N**| **I**| **G**|
|Key: privary | T| H| E| E| N| T| G|

Hence the key used to encrypt this plain text using Vigenere substitution is ```privacy``` giving the following plain text:

    The Enigma Machine was a cipher machine used to encrypt and
    decrypt secret messages. More precisely, Enigma was a family
    of related electro-mechanical rotor machines, comprising a
    variety of different models.

    The Enigma was used commercially, and was also adopted by
    the military and governmental services of a number of
    nations - most famously by Nazi Germany before
    and during Second World War.

    The German military model, the Wehrmacht Enigma is the
    version most commonly discussed. The machine has gained
    notoriety because Allied cryptologists were able to
    decrypt a large number of messages that had been
    enciphered on the machine. Decryption was made possible
    by polish cryptographers Marian Rejewski Jerzy Roycki and
    Henry Kzygalski from Cipher Bureau. Reconstruction and
    decryption methods were delivered from Poland to Britain and
    France. The intelligence gained through this source, codenamed
    ULTRA, was a significant aid to the Allied war effort. The exact
    influence of ULTRA is debated, but a typical assessment is
    that the end of the European war was hastened by two years
    because of the decryption of German ciphers.

# Appendix A - Kasiski Trigrams #

    Kasiski search for repeated strings of size 3.

    iym: 3| 399, 378
    znk: 3| 357, 528
    nke: 3| 357, 528
    keb: 2| 357
    ebr: 2| 357
    evs: 2| 861
    roy: 2| 329
    oyr: 2| 329
    yry: 2| 329
    agp: 2| 399
    gpx: 2| 399
    pxo: 2| 399
    svk: 3| 476, 385
    vkm: 4| 7, 469, 385
    kmy: 3| 476, 385
    myr: 3| 476, 385
    yrr: 3| 476, 385
    ptc: 2| 755
    tcr: 2| 755
    ccz: 2| 723
    kjn: 2| 536
    lzl: 2| 238
    qxa: 2| 714
    xan: 2| 714
    mca: 2| 278
    zvz: 3| 280, 308
    rkc: 2| 368
    kci: 2| 368
    cip: 2| 368
    dkd: 2| 459
    kdu: 2| 8
    vzz: 2| 511
    znv: 2| 77
    dum: 2| 294
    vft: 3| 175, 357
    aps: 2| 72
    sgb: 2| 210
    cls: 2| 483
    sni: 2| 119
    nin: 2| 553
    ina: 2| 553
    kxc: 2| 112
    xcq: 2| 112
    cqo: 2| 112
    qoa: 2| 112
    oat: 2| 112
    atw: 2| 112
    pel: 2| 413
    cge: 2| 481
    atj: 2| 655
    jnu: 2| 364
    tkp: 2| 366
    gvi: 2| 154
    iid: 2| 294
    otj: 2| 475
    tjc: 5| 42, 147, 280, 112
    qnt: 2| 483
    ntj: 2| 483
    kvz: 2| 301
    iql: 2| 266
    ght: 2| 188
    epz: 2| 308
    pzv: 3| 280, 28
    vzd: 2| 308
    mns: 2| 357
    zep: 2| 357
    epc: 2| 107
    csv: 2| 385
    rrx: 2| 385
    rxf: 2| 385
    xfv: 2| 385
    epn: 2| 91
    png: 2| 91
    ngb: 2| 91
    zii: 2| 21
    afi: 2| 28
    hpd: 2| 63
    pdd: 2| 63
    xjl: 2| 138
    icg: 2| 205
